﻿namespace LeetCode.Exe.RemoveNthNodeFromEnd
{
    public class ListNode
    {
        public int val;
        public ListNode next;

        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    public class Solution
    {
        public ListNode RemoveNthFromEnd(ListNode head, int n)
        {
            ListNode prev = null;
            var forDelete = head;
            var current = head;
            var count = 0;
            while (current != null)
            {
                if (count < n + 1)
                {
                    count++;
                }

                if (count == n + 1)
                {
                    prev = forDelete;
                    forDelete = forDelete.next;
                }

                current = current.next;
            }

            if (prev != null)
            {
                prev.next = forDelete.next;
            }
            else
            {
                return forDelete.next;
            }

            return head;
        }
    }
}