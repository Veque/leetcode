﻿using System.Collections.Generic;

namespace LeetCode.Exe.SubarraySumEqualsK
{
    public class Solution
    {
        public int SubarraySum(int[] nums, int k)
        {
            var hashmap = new Dictionary<int, int>(nums.Length);
            hashmap[0] = 1;
            var count=0;
            var sum = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                var element = nums[i];
                sum += element;
                hashmap.TryGetValue(sum - k, out var toCount);
                count += toCount;
                hashmap.TryGetValue(sum, out var n);
                hashmap[sum] = n + 1;
            }

            return count;
        }
    }
}