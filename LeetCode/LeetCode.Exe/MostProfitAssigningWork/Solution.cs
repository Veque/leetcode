﻿using System;

namespace LeetCode.Exe.MostProfitAssigningWork
{
    public class Solution
    {
        public int MaxProfitAssignment(int[] difficulty, int[] profit, int[] worker)
        {
            var sortedDif = new DifWithIndex[difficulty.Length];
            for (int i = 0; i < difficulty.Length; i++)
            {
                sortedDif[i].Difficulty = difficulty[i];
                sortedDif[i].Index = i;
            }

            Array.Sort(sortedDif, (dif1, dif2) => dif1.Difficulty.CompareTo(dif2.Difficulty));

            var maxProfit = 0;
            for (int i = 0; i < sortedDif.Length; i++)
            {
                ref var dif = ref sortedDif[i];
                var prof = profit[dif.Index];
                if (prof > maxProfit)
                {
                    maxProfit = prof;
                }

                dif.MaxProfit = maxProfit;
            }

            var result = 0;
            for (int i = 0; i < worker.Length; i++)
            {
                var w = worker[i];
                var prof = BinarySearch(sortedDif, w);
                result += prof;
            }

            return result;
        }

        private int BinarySearch(DifWithIndex[] difficulty, int worker)
        {
            if (difficulty[0].Difficulty > worker)
            {
                return 0;
            }

            var start = 0;
            var end = difficulty.Length - 1;
            var index = (end + start) / 2;

            while (start != end)
            {
                var v = difficulty[index];
                if (end - start > 1)
                {
                    if (v.Difficulty > worker)
                    {
                        end = index - 1;
                    }
                    else
                    {
                        start = index;
                    }
                }
                else
                {
                    if (difficulty[end].Difficulty <= worker)
                    {
                        return difficulty[end].MaxProfit;
                    }
                    return difficulty[start].MaxProfit;
                }
                

                index = (end + start) / 2;
            }

            return difficulty[index].MaxProfit;
        }

        private struct DifWithIndex
        {
            public int Difficulty;
            public int Index;
            public int MaxProfit;
        }
    }
}