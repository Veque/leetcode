﻿using System;
using System.Diagnostics;

namespace LeetCode.Exe
{
    public class Timer:IDisposable
    {
        private Stopwatch _watch = Stopwatch.StartNew();
        public void Dispose()
        {
            Console.WriteLine(_watch.Elapsed.ToString("g"));
        }
    }
}