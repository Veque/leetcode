﻿using System;
using LeetCode.Exe.MostProfitAssigningWork;

namespace LeetCode.Exe
{
    class Program
    {
        static void Main(string[] args)
        {
            var solution = new Solution();
            var d = new int[] {2, 4, 6,8,10};
            var p = new int[] {10, 20, 30,40,50};
            var w = new int[] {4, 5, 6,7};

            int result = 0;
            using (new Timer())
            {
                result = solution.MaxProfitAssignment(d, p, w);
            }
            
            Console.WriteLine(result);
        }
    }
}