﻿using System.Collections.Generic;

namespace LeetCode.Exe.SubarraySumDivisibleByK
{
    public class Solution
    {
        public int SubarraysDivByK(int[] A, int k)
        {
            var hashmap = new Dictionary<int, int>(k);
            hashmap[0] = 1;
            var count=0;
            var sum = 0;
            for (int i = 0; i < A.Length; i++)
            {
                var element = A[i];
                sum += element;
                var sumk = sum % k;
                
                hashmap.TryGetValue(sumk, out var toCount);
                count += toCount;
                
                hashmap.TryGetValue(sumk-k, out toCount);
                count += toCount;
                
                hashmap.TryGetValue(sumk+k, out toCount);
                count += toCount;

                hashmap.TryGetValue(sumk, out var n);
                hashmap[sumk] = n + 1;
            }

            return count;
        }
    }
}